// Variable to set thee size of the flag to draw.
// This is the single input required for the drawing.
int   flagHeight      = 390;

// Variables for the sizes of all objects which are proportional to the size of the flag.
// These variables should not be changed.
int   flagWidth       = round(flagHeight*1.9);
float barHeight       = flagHeight/13;
float heightOfUnion   = barHeight*7;
float widthOfUnion    = flagHeight*0.76;
float headWidth       = flagHeight/1.35;
float noseWidth       = headWidth/7.5;
float eyeWidth        = headWidth/5;
float eyeHeight       = eyeWidth*1.7;
float eyeBorderWidth  = eyeWidth*1.2;
float eyeBorderHeight = eyeBorderWidth*1.6;
float pupilWidth      = eyeWidth/3;
float pupilHeight     = eyeHeight/2;
float eyeBrowWidth    = eyeWidth*1.1;
float hairWidth       = headWidth/3;
float mouthWidth      = headWidth*0.75;
float mouthHeight     = mouthWidth*0.8;
float teethWidth      = mouthWidth*0.9;


void settings() {
  // since we are using a variable here, we have to set the window size in settings(), NOT in setup()
  size(flagWidth, flagHeight);
}


void setup() {
  // disable strokes on all objects
  noStroke();

  // fuction to draw horizontal stripes
  // it takes one parameter, the number of stripes to draw
  makeStripes(13);

  // fuction to draw the box/union on the american flag
  drawUnion();

  // function to draw the 45th president of the united states
  drawClown();
}


// A function to made a series of horizontal stripes. It takes one parameter. the number of stripes to create.
void makeStripes(int numStripes) {

  // Generate the stripes with a for loop
  for (int i = 0; i < numStripes; i++) {

    // To have stripes drawn down the page equally (no overlaps), we multiple the height of each stripe by i, we will use this for the Y parameter when we draw the stripes
    float barY = barHeight*i;

    // to have the stripes alteernate in colour, we check the loop index (i). if it is even, then make the stripe red, if it's not even, make it white.
    if (i % 2 == 0) {

      // fill the shape with red
      fill(0, 0, 0);

      // here we draw the stripe, using values from variables we made above
      rect(0, barY, flagWidth, barHeight);
    } else {

      // fill the shape with white
      fill(255, 255, 255);

      // here we draw the stripe, using values from variables we made above
      rect(0, barY, flagWidth, barHeight);
    }
  }
}


// function to draw the box/union on the upper left corner of the american flag
void drawUnion() {
  
  // fill the box with black
  fill(0, 0, 0);
  
  // draw rectangle using variable data/sizes defined above
  rect(0, 0, widthOfUnion, heightOfUnion);
  
  // set fill color for numbers to white
  fill(255, 255, 255);
  
  // set text size to be relatize to the height of the box/stripes
  textSize(barHeight*7);
  
  // draw text into the box
  text("45", pupilWidth/3, barHeight*6);
}


// a function to draw a likeness of the 45th preident of the united states
// everything drawn is proportional to the width of the flag
void drawClown() {

  // hair left
  fill(255, 127, 0);
  circle((flagWidth/2)-(headWidth/3), (height/2)-(headWidth/3), hairWidth);
  circle((flagWidth/2)-(headWidth/2.5), (height/2)-(headWidth/4), hairWidth);

  // hair right
  circle((flagWidth/2)+(headWidth/3), (height/2)-(headWidth/3), hairWidth);
  circle((flagWidth/2)+(headWidth/2.5), (height/2)-(headWidth/4), hairWidth);

  // head
  fill(241, 231, 104);
  circle(flagWidth/2, height/2, headWidth);

  // mouth
  noStroke();
  fill(255, 0, 0);
  ellipse(flagWidth/2, round((height/2)+(mouthHeight/6)), mouthWidth, mouthHeight);
  fill(255, 255, 255);
  ellipse(flagWidth/2, round((height/2)+(mouthHeight/9)), teethWidth, mouthHeight);
  fill(255, 0, 0);
  ellipse(flagWidth/2, round((height/2)+(mouthHeight/18)), mouthWidth, mouthHeight);
  fill(241, 231, 104);
  ellipse(flagWidth/2, round(height/2), mouthWidth*1.2, mouthHeight);

  // eye brows
  fill(0, 0, 0);
  ellipse(flagWidth/2-eyeWidth/1.55, height/2-pupilHeight, eyeBrowWidth, eyeBorderHeight);
  ellipse(flagWidth/2+eyeWidth/1.55, height/2-pupilHeight, eyeBrowWidth, eyeBorderHeight);
  fill(241, 231, 104);
  ellipse(flagWidth/2-eyeWidth/1.55, height/2-pupilHeight/1.1, eyeBrowWidth*1.2, eyeBorderHeight);
  ellipse(flagWidth/2+eyeWidth/1.55, height/2-pupilHeight/1.1, eyeBrowWidth*1.2, eyeBorderHeight);

  // eye border
  fill(0, 0, 255);
  ellipse(flagWidth/2-eyeBorderWidth/1.8, height/2-eyeBorderWidth/2, eyeBorderWidth, eyeBorderHeight);
  ellipse(flagWidth/2+eyeBorderWidth/1.8, height/2-eyeBorderWidth/2, eyeBorderWidth, eyeBorderHeight);

  // eye white
  fill(255, 255, 255);
  ellipse(flagWidth/2-eyeBorderWidth/1.8, height/2-eyeBorderWidth/2, eyeWidth, eyeHeight);
  ellipse(flagWidth/2+eyeBorderWidth/1.8, height/2-eyeBorderWidth/2, eyeWidth, eyeHeight);

  // eye pupil
  fill(0, 0, 0);
  ellipse(flagWidth/2-eyeBorderWidth/1.8, height/2-eyeBorderWidth/2, pupilWidth, pupilHeight);
  ellipse(flagWidth/2+eyeBorderWidth/1.8, height/2-eyeBorderWidth/2, pupilWidth, pupilHeight);

  // nose
  fill(255, 0, 0);
  circle(flagWidth/2, height/2+pupilWidth/0.7, noseWidth);
}